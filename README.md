# Gocorna Website Task

Implement design according [Figma design](https://www.figma.com/file/8OmsbdXHlnVZGknLy2i6Mv/Gocorna-Website-(Community)?type=design&node-id=0-1&mode=design&t=UvXg0lT2ngRk6hGr-0)

## Requirements

- Stack: HTML, CSS, JS (no frameworks)
- TypeScript optional
- Usage of JS bundlers (Vite, Webpack, rollout, etc)
