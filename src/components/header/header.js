import { createHeading } from '../heading';
import Button from '../button/button';
import logoImg from '../../assets/small-logo.svg';
import './header.css';

export default function CreateHeader() {
    const linkTexts = ['HOME', 'FEATURES', 'SUPPORT', 'CONTACT US'];

    const header = document.createElement('header');
    header.classList.add('hero-header');

    const logo = document.createElement('div');
    logo.classList.add('logo');
    logo.innerHTML = `<a href="/"><img src=${logoImg} alt="logo"></a>`;

    const logoText = createHeading('red', [
        { text: 'Go', color: 'black' },
        { text: 'Corona', color: 'red' }
    ]);
    logoText.classList.add('logo-text');
    logo.appendChild(logoText);

    const navLinks = document.createElement('div');
    navLinks.classList.add('nav-links');

    linkTexts.forEach((text, i) => {
        const link = document.createElement('a');
        link.href = `#link${i + 1}`;
        link.textContent = text;
        navLinks.appendChild(link);
    });

    const actionButton = Button({ className: 'secondary', text: 'DOWNLOAD' });
    actionButton.classList.add('header-btn');

    header.appendChild(logo);
    header.appendChild(navLinks);
    header.appendChild(actionButton);

    return header;
}
