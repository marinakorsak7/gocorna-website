import './button.css';

export default function Button(props) {
    const {className, text} = props
    const button = document.createElement('button');
    button.classList.add(`${className}`);
    button.innerHTML = text;
    return button;
}

