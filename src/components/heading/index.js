import "./styles.css";

export  function createHeading(color, textParts) {
    const heading = document.createElement('h2');

    textParts.forEach(part => {
        const span = document.createElement('span');
        span.innerText = part.text;

        if (part.color === 'red') {
            span.className = 'red-text';
        } else if (part.color === 'blue') {
            span.className = 'blue-text';
        }

        heading.appendChild(span);
    });

    return heading;
}