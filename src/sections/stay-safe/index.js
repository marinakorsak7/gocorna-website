import "./styles.css";
import {createHeading} from "../../components/heading";
import Button from "../../components/button/button";
import Image from "../../assets/sta-safe-section-img.png";

export function StaySafe() {
    const section = document.createElement('section');
    section.id = "stay-safe";
    const heading = createHeading("red", [
        {color: "black", text: "Stay safe with \n Go"},
        {color: "red", text: "Corona."}
    ]);

    const button = Button({className:"primary", text:"FEATURES"});

    section.innerHTML = `
        <div class="stay-safe__img">
            <img src="${Image}" alt="Stay Safe Image">
        </div>
        <div class="stay-safe__content">
            <p class="stay-safe__content-paragraph">
                24x7 Support and user friendly mobile platform to bring <br> 
                healthcare to your fingertips. Signup and be a part of <br> 
                the new health culture.
            </p>
            <div></div>
        </div>   
    `;

    const content = section.querySelector(".stay-safe__content");
    content.prepend(heading);
    content.append(button);


    return section;
}