import './card-item.css';

export default function cardItem(props) {
    const {iconUrl, cardHeading, cardText} = props;
    return `
      <div class="card-item">
          <img class="card-item-img" src=${iconUrl} alt="card-icon">
          <p class="card-item-heading">${cardHeading}</p>
          <p class="card-item-text">${cardText}</p>
      </div>
    `
}
