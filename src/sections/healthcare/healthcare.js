import './healthcare.css';
import {createHeading} from '../../components/heading';
import doctorIcon from '../../assets/fingertips-icon-doctor.svg';
import manIcon from '../../assets/fingetrips-icon-man.svg';
import heartIcon from '../../assets/fingertips-icon-heart.svg';
import cardItem from './card-item/card-item';
import googlePlayIcon from '../../assets/googleplay-icon.png';
import appleIcon from '../../assets/appstore-icon.png';

const data = [
    { text: 'Healthcare ', color: 'red' },
    { text: 'at your Fingertips.', color: 'black' }
];

const cardsData = [
    {iconUrl: manIcon, cardHeading: 'Symptom Checker', cardText: 'Check if you are infected by COVID-19 with our Symptom Checker'},
    {iconUrl: doctorIcon, cardHeading: '24x7 Medical support', cardText: 'Consult with 10,000+ health workers about your concerns.'},
    {iconUrl: heartIcon, cardHeading: 'Conditions', cardText: 'Bringing premium healthcare features to your fingertips.'}
]

export default function Healthcare() {
    const section = document.createElement('section');
    section.classList.add('section-healthcare');
    section.setAttribute('id','container');

    const headingHealthcare = createHeading('red', data);

    section.innerHTML = `
      <p class="healthcare-text">Bringing premium healthcare features to your fingertips. User friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.</p>
      <div class="healthcare-cards">
          ${cardsData.map((item) => cardItem({iconUrl: item.iconUrl, cardHeading: item.cardHeading, cardText: item.cardText})).join('')}
      </div>
      <div class="healthcare-links">
          <div class="google">
            <img src=${googlePlayIcon} alt="google-play-icon">
          </div>
            <div class="apple">
            <img src=${appleIcon} alt="apple-icon">
          </div>
      </div>
`

    section.prepend(headingHealthcare);

    return section;
}
