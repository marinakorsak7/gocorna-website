import './data-item.css';

export default function DataItem(props) {
    const { quantaty, text } = props;
    return `
      <div class="data-item">
          <p class="data-item-value">${quantaty}</p>
          <p class="data-item-text">${text}</>
      </div>
    `;
}
