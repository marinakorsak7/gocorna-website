import './talk-to-experts.css';
import DataItem from './data-item/data-item';
import { createHeading } from '../../components/heading';
import Button from '../../components/button/button';
import videoImg from '../../assets/experts-video.png';
import playVideoIcon from '../../assets/play-vide.svg';

const expertsData = [
    {quantaty: "2m", text: "Users"},
    {quantaty: "78", text: "Countries"},
    {quantaty: "10,000+", text: "Medical experts"}
];

const data = [
    { text: 'Talk to ', color: 'black' },
    { text: 'experts.', color: 'blue' }
];

export default function TalkToExperts() {
    const section = document.createElement('section');
    section.classList.add('section-experts');
    section.setAttribute('id','container');

    section.innerHTML = `
       <div class="experts-numbers">
          <div class="experts-numbers-data">
             ${expertsData.map((item) => DataItem({quantaty: item.quantaty, text: item.text})).join('')}
          </div>   
       </div>
       <div class="experts-content">
          <div class="experts-info">
              <p class="section-info-text">Book appointments or submit queries into thousands of forums concerning health issues and prevention against noval Corona Virus.</p>
          </div>
          <div class="experts-video">
              <img src=${videoImg} alt="video-player"/>
              <img class="play-video" src=${playVideoIcon} alt="play-video">
          </div>
       </div>
    `;

    const headingExperts = createHeading('blue', data);
    const expertsInfoDiv = section.querySelector('.experts-info');
    expertsInfoDiv.prepend(headingExperts);

    section.querySelector('h2').classList.add('experts-heading');

    const btn = Button({className: 'primary', text: 'Features'});
    btn.classList.add('experts-btn');
    expertsInfoDiv.append(btn);

    return section;
}

