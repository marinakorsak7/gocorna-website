import './styles.css';
import { createHeading } from '../../components/heading';
import Button from '../../components/button/button';
import playVideoIcon from '../../assets/video-icon.svg';

export default function TakeCare() {
    const section = document.createElement('section');
    section.classList.add('section-hero');

    const heroContent = document.createElement('div');
    heroContent.classList.add('hero-content');
    section.appendChild(heroContent);

    const heroInfo = document.createElement('div');
    heroInfo.classList.add('hero-info');
    heroContent.appendChild(heroInfo);

    const infoText = document.createElement('p');
    infoText.classList.add('section-info-text');
    infoText.textContent = 'All in one destination for COVID-19 health queries.  Consult 10,000+ health workers about your concerns.';
    heroInfo.appendChild(infoText);

    const videoInfo = document.createElement('div');
    videoInfo.classList.add('video-info');
    heroContent.appendChild(videoInfo);

    const videoLink = document.createElement('a');
    videoLink.href = '#video-link';
    videoInfo.appendChild(videoLink);

    const playIcon = document.createElement('img');
    playIcon.classList.add('play-icon');
    playIcon.src = playVideoIcon;
    playIcon.alt = 'Play Video';
    videoLink.appendChild(playIcon);

    const videoTexts = document.createElement('div');
    videoTexts.classList.add('video-texts');
    videoInfo.appendChild(videoTexts);

    const staySafeText = document.createElement('p');
    staySafeText.classList.add('stay-safe-text');
    staySafeText.textContent = 'Stay safe with GoCorona';
    videoTexts.appendChild(staySafeText);

    const watchvideoLink = document.createElement('a');
    watchvideoLink.classList.add('watch-link');
    watchvideoLink.href = '#video-link';
    videoTexts.appendChild(watchvideoLink);

    const watchText = document.createElement('p');
    watchText.classList.add('watch-text');
    watchText.textContent = 'WATCH VIDEO';
    watchvideoLink.appendChild(watchText);

    const headingHero = createHeading('blue', [
        { text: 'Take care of yours family’s ', color: 'black' },
        { text: 'health', color: 'blue' }
    ]);
    heroInfo.prepend(headingHero);

    const btn = Button({ className: 'primary', text: 'GET STARTED' });
    btn.classList.add('hero-btn');
    heroInfo.appendChild(btn);

    section.querySelector('h2').classList.add('hero-heading');

    const heroBg = document.createElement('div');
    heroBg.classList.add('hero-bg');
    section.appendChild(heroBg);

    return section;
}
