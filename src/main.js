import "./styles.css";
import TalkToExperts from './sections/talk-to-experts/talk-to-experts';
import {StaySafe} from "./sections/stay-safe";
import Healthcare from './sections/healthcare/healthcare';
import CreateHeader from './components/header/header';
import TakeCare from './sections/hero/';

const app = document.querySelector("#app"); // div#app

// header
const header = CreateHeader();
app.append(header);

// Take Care section
const takeCare = TakeCare();
app.append(takeCare);

// stay Safe section
const  staySafe = StaySafe();
app.append(staySafe);

// Talk to experts section
const talkToExperts = TalkToExperts();
app.append(talkToExperts);

// Healthcare
const healthcare = Healthcare();
app.append(healthcare);
